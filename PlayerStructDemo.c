#include <stdio.h>

//Declaring a debuff enum
enum EDebuff
{
	None = 0,
	Poisoned = 0b1, 
	Dazed = 0b1 << 1, 
	Chilled = 0b1 << 2,
	Burnt = 0b1 << 3
};

//Declaring a person struct
struct Person
{
	char surname[16];
	char firstname[16];
	int age;
	float height;
};

//Declaring a player struct
struct Player
{
	Person player_details;
	EDebuff debuff_state;
	int hitpoint;
	int max_hp;
};

void PrintPlayerDetails( Player p )
{
	//Study the following statements to learn how a struct member can be accessed from a variable
	printf("Player name: %s %s\n", p.player_details.firstname, p.player_details.surname);
	printf("Player age: %d\n", p.player_details.age);
	printf("Player max hp: %d\n", p.max_hp);
}

int GetPlayerCurrentHP( Player p )
{
	int current_hp = 0;
	//TODO: return the playper p's current hit point
	//Remember Player is a struct
	//use synatx variable.member to access a member 
	
	return current_hp;
}

bool IsPlayerPoisoned( Player p )
{
	bool ispoisoned = false;
	
	//TODO: check if the player p is poisoned
	//return true if it is, false otherwise

	return ispoisoned;
}

int main( int argc, char **argv )
{
	//Declare and initialise two players
	Player p1 = {
		{"Norris", "Chuck", 300, 1.9f}, //init the person portion of the struct
		(EDebuff)(Poisoned | Dazed), //init the EDebuff member
		110, //init current hit point
		100 //init max hit point
	};

	Player p2 = {
		{"Jenkins", "Leeroy", 999, 0.9f}, //init the person portion of the struct
		(EDebuff)(Poisoned | Dazed | Chilled | Burnt), //init the EDebuff member
		1, //init current hit point
		10 //init max hit point
	};

	//Print the details of p1
	PrintPlayerDetails(p1);
	//Print the details of p2
	PrintPlayerDetails(p2);

	//Test statements for this exercise
	printf("Player %s %s current HP: %d\n", p1.player_details.firstname, p1.player_details.surname, GetPlayerCurrentHP(p1));
	printf("Player %s %s current HP: %d\n", p2.player_details.firstname, p2.player_details.surname, GetPlayerCurrentHP(p2));

	if ( IsPlayerPoisoned( p1 ) )
	{
		printf("Player %s %s is poisoned\n", p1.player_details.firstname, p1.player_details.surname);
	}

	return 0;
}
