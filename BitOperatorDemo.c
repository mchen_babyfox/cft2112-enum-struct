#include <stdio.h>

enum EDebuff
{
	None = 0,
	Poisoned = 0b1, 
	Dazed = 0b1 << 1, 
	Chilled = 0b1 << 2,
	Burnt = 0b1 << 3
};

int main( int argc, char **argv )
{
	EDebuff states = None;	

	//Print enum values as hex values
	printf("EDebuff: Poisoned %#x\n", Poisoned);
	printf("EDebuff: Dazed %#x\n", Dazed);
	printf("EDebuff: Chilled %#x\n", Chilled);
	printf("EDebuff: Burnt %#x\n", Burnt);
	printf("Question: Which bit correspond to which debuff state?\n\n\n");

	//Set status
	printf("Question: Write down the following printed hex decimal value in binary\n");
	printf("Check if each bit corresponds to the debuff status?\n\n");
	printf("states = None \n");
	printf("Current State: %#x\n\n", states);

	states = (EDebuff)(states | Burnt);
	printf("states = states | Burnt \n");
	printf("Current State: %#x\n\n", states);
	
	states = (EDebuff)(states | Poisoned);
	printf("states = states | Poisoned  \n");
	printf("Current State: %#x\n\n", states);
	
	states = (EDebuff)(states & (~Burnt));
	printf("states = states & (~Burnt) \n");
	printf("Current State: %#x\n\n\n\n", states);

	//Check status
	printf("Is poison on? states & Poisoned\n");
	printf("states & Poisoned: %#x\n\n", states & Poisoned);
	
	printf("Is poison on? states & Dazed\n");
	printf("states & Dazed: %#x\n\n", states & Dazed);
	
	printf("Is poison on? states & Chilled\n");
	printf("states & Chilled: %#x\n\n", states & Chilled);
	
	printf("Is poison on? states & Burnt\n");
	printf("states & Poisoned: %#x\n\n", states & Burnt);

	return 0;
}

