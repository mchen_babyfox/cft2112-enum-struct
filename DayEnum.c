#include <stdio.h>

enum Day{ 
	Sunday = 3, 
	Monday ,
	Tuesday , 
	Wednesday = 8, 
	Thursday , 
	Friday , 
	Saturday
};

int main ( int argc, char **argv )
{
	enum Day day1 = Tuesday;
	enum Day day2 = Friday;
	int Day tillweekend = day2 - day1;

	printf("Tuesday is %d\n", day1);
	printf("Friday is %d\n", day2);
	printf("Days till weekend %d\n", tillweekend);
	
	return 0;
}
